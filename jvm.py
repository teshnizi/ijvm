from math import log2
from bitstring import Bits
class jvm:
    pc = 0
    ar = 0
    dr = 0
    ir = 0
    opr = 0
    lv = 96
    sp = 64
    cpp = 96
    tos = 0
    ac = 0
    b = 0

    def print(self):
        print("JVM Registers:")
        print("pc ", self.pc)
        print("ar ", self.ar)
        print("dr ", self.dr)
        print("ir ", self.ir)
        print("opr ", self.opr)
        print("lv ", self.lv)
        print("sp ", self.sp)
        print("cpp ", self.cpp)
        print("tos ", self.tos)
        print("ac ", self.ac)
        print("b ", self.b)
        print("\n")

    def reset(self):
        self.pc = 0
        self.ar = 0
        self.dr = 0
        self.ir = 0
        self.opr = 0
        self.lv = 96
        self.sp = 64
        self.cpp = 96
        self.tos = 0
        self.ac = 0
        self.b = 0

    #TODO use sets not dictionary maybe??
    def alu_op(self, source='pc',  destination=[],
               incrementby1=[], incrementby4=[],
               decrementby1=[], decrementby4=[], assign=[],
               isext8=[], isext16=[], read=False, write=False,
               s_z=False, s_n=False,
               s_assign=[], shamt=0, operation="bus"):

        bus = eval('self.' + source)
        tac = self.ac
        if type(destination) != list:
            destination = [destination]

        if type(assign) != list:
            assign = [assign]

        if type(decrementby4) != list:
            decrementby4 = [decrementby4]

        if type(decrementby1) != list:
            decrementby1 = [decrementby1]

        if type(incrementby4) != list:
            incrementby4 = [incrementby4]

        if type(incrementby1) != list:
            incrementby1 = [incrementby1]

        if type(isext8) != list:
            isext8 = [isext8]

        if type(isext16) != list:
            isext16 = [isext16]

        if type(s_assign) != list:
            s_assign = [s_assign]

        sig_list = dict()
        sig_list["bus"] = source
        sig_list["alu"] = operation

        for i in assign:
            x, y = i
            exec('self.' + x + ' = ' + str(y))
            sig_list[x + '_mld'] = 1

        talu = 0
        if operation == "sum":
            talu = tac + bus

        elif operation == "sub":
            talu = bus - tac

        elif operation == "bus":
            if source == 'ac':
                sig_list['alu'] = 'ac'
            talu = bus

        elif operation == 'ac':
            talu = tac

        elif operation == 'or':
            talu = tac|bus
        else:
            raise NameError("alu operation undefined")
        if s_n:
            sig_list['b_ld'] = 1
            sig_list['b_neg'] = 1
            if talu < 0:
                self.b = 1
            else:
                self.b = 0
        if s_z:
            sig_list['b_ld'] = 1
            if talu == 0:
                self.b = 1
            else:
                self.b = 0

        for i in destination:
            sig_list[i + 'ld'] = 1
            rvalue = talu
            if i in isext8:
                sig_list[i + '_isext8'] = 1;
                b = Bits(int=talu, length=32).bin[24:32]
                rvalue = Bits(bin=b).int


            if i in isext16:
                sig_list[i + '_isext8'] = 1;
                rvalue = Bits(bin=Bits(int=talu, length=32).bin[16:32]).int

            exec('self.' + i + ' = rvalue << ' + str(shamt))

        if shamt > 0:
                sig_list['shamt'] = int(log2(shamt))



        for i in incrementby1:
            exec('self.' + i + ' += 1')
            sig_list[i + '_inc1'] = 1

        for i in incrementby4:
            exec('self.' + i + ' += 4')
            sig_list[i + '_inc4'] = 1

        for i in decrementby1:
            exec('self.' + i + ' -= 1')
            sig_list[i + '_dec1'] = 1

        for i in decrementby4:
            exec('self.' + i + ' -= 4')
            sig_list[i + '_inc4'] = 1

        if read:
            sig_list['start'] = 1;

        if write:
            sig_list['start'] = 1;
            sig_list['write'] = 1;

        return sig_list

