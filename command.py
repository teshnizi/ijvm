
from bitstring import Bits
import re
opcode_dict = {
    'nop': 0x00,
    'iadd': 0x60,
    'isub': 0x64,

    'bitpush': 0x10,

    'iload': 0x15,
    'istore': 0x36,

    'iinc': 0x84,

    'goto': 0xa7,
    'ifeq': 0x99,
    'iflt': 0x9b,
    'if_icmpeq': 0x9f

}
opcode_length = {
    'nop': 1,
    'iadd': 1,
    'isub': 1,

    'bitpush': 2,

    'iload': 2,
    'istore': 2,

    'iinc': 3,

    'goto': 3,
    'ifeq': 3,
    'iflt': 3,
    'if_icmpeq': 3
}
param_len = {
    'bitpush': [1],
    'iload': [1],
    'istore': [1],
    'iinc': [1, 1],
    'goto': [2],
    'ifeq': [2],
    'iflt': [2],
    'if_icmpeq': [2]
}
def pars(i):
    if type(i) == int:
        return i
    elif i[-1].upper() == 'H':
        return int(i[0: -2], base=16)
    else:
        return int(i)


def mulpars(num, l):
    x = pars(num)
    acc = []
    x = Bits(int=x, length=l*8).bin
    parts = [x[i:i + 8] for i in range(0, l*8, 8)]
    for i in parts:
        acc.append(Bits(bin=i).int)
    return acc


def asm(x):
    y = x.split('\n')
    z = list()
    for i in y:
        z.extend(i.split(' '))

    t = list()
    lookup_table = dict()
    for pass2 in range(2):

        pcode = []
        tpc = 0
        pc = 0
        comands = []
        double = False
        for i in z:
            if i != '':
                if i in opcode_dict:

                    if pass2:

                        t.append(opcode_dict[i])
                    if opcode_length[i] > 1:
                        pcode = param_len[i]
                    pc = tpc
                    tpc += 1
                    comands.append([i])

                elif pcode != []:
                    if not double:
                        comands[-1].append(i)
                    double = False
                    if i in lookup_table:
                        if pass2:
                            t.extend(mulpars(lookup_table[i] - pc, pcode[0]))
                    else:
                        if pass2:
                            t.extend(mulpars(i, pcode[0]))
                    tpc += pcode[0]
                    pcode = pcode[1:]
                elif i.upper() == "D":
                    pcode = [2]
                    double = True
                else:
                    if i[-1] != ':':
                        if pass2:
                            t.append(pars(i))
                        tpc += 1
                    else:
                        lookup_table[i[:-1]] = tpc


    return t, comands


class updateJVM:
    def __init__(self, jvm, mem):
        self.jvm = jvm
        self.mem = mem
        self.delays = list()
        self.signals = list()
        self.jvm.ir = 0
        self.trace = True
        self.step = 0
        # self.total_delay = 0
        return

    def t(self):
        if self.trace:
            print("internals")
            self.jvm.print()

    def print_status(self):
        # print(self.step)
        # print(len(self.signals))
        if ( self.step >= len(self.signals)):
            print("****************************************")
        print("-------------------\nStep " + str(self.step) + ":")
        self.jvm.print()
        print("Signals:")
        print(self.signals[self.step])
        print("Delay:")
        print(self.delays[self.step])
        self.step = self.step + 1
        print("-------------------\n")

    def updateJVM(self):
        self.step = 0
        self.delays = list()
        self.signals = list()

        # 0 000: ar_ld data_pc
        self.signals.append(self.jvm.alu_op(source='pc', destination='ar'))
        self.delays.append(0)
        self.print_status()


        # 1m_read pc_inc ar_inc
        delay, opcode = self.mem.read1(self.jvm.ar)
        self.signals.append(self.jvm.alu_op(incrementby1=['pc', 'ar'], read=True))
        self.delays.append(delay)
        self.print_status()

        # 2 ir_ld sec_inst
        self.signals.append(self.jvm.alu_op(assign=('ir', opcode)))
        self.delays.append(0)
        self.print_status()

        if (self.jvm.ir == opcode_dict['iadd']) | (self.jvm.ir == opcode_dict['isub']):
            self.iadd_isub()
        elif self.jvm.ir == opcode_dict['bitpush']:
            self.bitpush()
        elif self.jvm.ir == opcode_dict['iload']:
            self.iload()
        elif self.jvm.ir == opcode_dict['istore']:
            self.istore()
        elif self.jvm.ir == opcode_dict['iinc']:
            self.iinc()
        elif self.jvm.ir == opcode_dict['goto']:
            self.goto()
        elif (self.jvm.ir == opcode_dict['ifeq']) | (self.jvm.ir == opcode_dict['iflt']):
            self.popjump()
        elif self.jvm.ir == opcode_dict['if_icmpeq']:
            self.pop2jump()
        elif self.jvm.ir == opcode_dict['nop']:
            return
        else:
            raise NameError("opcode not found" + str(self.jvm.ir))

    def iadd_isub(self):
        # ar_ld data_sp sp_dec4
        self.signals.append(self.jvm.alu_op(source='sp', destination='ar',
                                            decrementby4='sp'))
        self.delays.append(0)
        self.print_status()

        # m_read ar_dec4
        delay, v = self.mem.read4(self.jvm.ar)
        self.signals.append(self.jvm.alu_op(decrementby4='ar', read=True))
        self.delays.append(delay)
        self.print_status()

        # dr_self.mem m_read
        delay, v1 = self.mem.read4(self.jvm.ar)
        self.signals.append(self.jvm.alu_op(assign=('dr', v), read=True))
        self.delays.append(delay)
        self.print_status()

        # r_ld data_dr dr_self.mem
        self.signals.append(self.jvm.alu_op(source='dr', destination='ac',
                                            assign=('dr', v1)))
        self.delays.append(0)
        self.print_status()

        if self.jvm.ir == opcode_dict['iadd']:
            self.signals.append(self.jvm.alu_op(source='dr', destination='dr',
                                                operation='sum'))
        else:
            self.signals.append(self.jvm.alu_op(source='dr', destination='dr',
                                                operation='sub'))
        self.delays.append(0)
        self.print_status()

        delay = self.mem.write(self.jvm.ar, self.jvm.dr)
        self.signals.append(self.jvm.alu_op(write=True))
        self.delays.append(delay)
        self.print_status()

    def bitpush(self):

        # m_read pc_inc ar_inc sp_inc4
        delay, opr = self.mem.read1(self.jvm.ar)
        self.signals.append(self.jvm.alu_op(incrementby1=['pc', 'ar'],
                                            incrementby4='sp', read=True))
        self.delays.append(delay)
        self.print_status()

        # opr_ld data_sp ar_ld
        self.signals.append(self.jvm.alu_op(assign=('opr', opr),
                                            source='sp', destination='ar'))
        self.delays.append(0)
        self.print_status()

        # data_opr dr_ld dr_isext8
        self.signals.append(self.jvm.alu_op(source='opr', destination='dr', isext8='dr'))
        self.delays.append(0)
        self.print_status()

        # data_opr dr_ld dr_isext8
        delay = self.mem.write(self.jvm.ar, self.jvm.dr)
        self.signals.append(self.jvm.alu_op(write=True))
        self.delays.append(delay)
        self.print_status()


    def iload(self):
        # m_read ar_inc pc_inc sp_inc4
        delay, x = self.mem.read1(self.jvm.ar)
        self.signals.append(self.jvm.alu_op(read=True, incrementby1=['ar', 'pc'], incrementby4='sp'))
        self.delays.append(delay)
        self.print_status()

        # opr_ld
        self.signals.append(self.jvm.alu_op(assign=('opr', x)))
        self.delays.append(0)
        self.print_status()

        # r_ld shifter_2 data_opr
        self.signals.append(self.jvm.alu_op(shamt=2, source='opr', destination='ac'))
        self.delays.append(0)
        self.print_status()

        # ar_ld data_lv alu_sum
        self.signals.append(self.jvm.alu_op(source='lv', destination='ar', operation='sum'))
        self.delays.append(0)
        self.print_status()

        # m_read ar_ld data_sp
        d, v = self.mem.read4(self.jvm.ar)
        self.signals.append(self.jvm.alu_op(source='sp', destination='ar', read=True))
        self.delays.append(d)
        self.print_status()

        # dr_self.mem
        self.signals.append(self.jvm.alu_op(assign=('dr', v)))
        self.delays.append(0)
        self.print_status()

        # m_write
        d = self.mem.write(self.jvm.ar, self.jvm.dr)
        self.signals.append(self.jvm.alu_op(write=True))
        self.delays.append(d)
        self.print_status()

    def istore(self):
        # m_read pc_inc
        d, v = self.mem.read1(self.jvm.ar)
        self.signals.append(self.jvm.alu_op(read=True, incrementby1='pc'))
        self.delays.append(d)
        self.print_status()

        # opr_ld ar_ld data_sp sp_dec4
        self.signals.append(self.jvm.alu_op(assign=('opr', v), source='sp', destination='ar', decrementby4='sp'))
        self.delays.append(0)
        self.print_status()

        # r_ld data_opr m_read shamt=2
        d, v = self.mem.read4(self.jvm.ar)
        self.signals.append(self.jvm.alu_op(source='opr', destination='ac', read=True, shamt=2))
        self.delays.append(d)
        self.print_status()

        # ar_ld data_lv alu_sum
        self.signals.append(self.jvm.alu_op(assign=('dr', v), source='lv', operation='sum', destination='ar'))
        self.delays.append(0)
        self.print_status()

        # m_write
        d = self.mem.write(self.jvm.ar, self.jvm.dr)
        self.signals.append(self.jvm.alu_op())
        self.delays.append(d)
        self.print_status()

    def iinc(self):
        # m_read ar_inc pc_inc
        d, v = self.mem.read1(self.jvm.ar)
        self.signals.append(self.jvm.alu_op(read=True, incrementby1=['ar', 'pc']))
        self.delays.append(d)
        self.print_status()

        # opr_ld ar_inc pc_inc m_read
        d1, v1 = self.mem.read1(self.jvm.ar)
        self.signals.append(self.jvm.alu_op(assign=('opr', v), incrementby1=['ar', 'pc'], read=True))
        self.delays.append(d1)
        self.print_status()

        # r_ld data_opr m_read opr_ld
        self.signals.append(self.jvm.alu_op(source='opr', destination='ac', assign=('opr', v1), shamt=2))
        self.delays.append(0)
        self.print_status()

        # ar_ld data_lv alu_sum shamt=2
        self.signals.append(self.jvm.alu_op(source='lv', destination='ar', operation='sum'))
        self.delays.append(0)
        self.print_status()

        # m_read r_ld data_opr
        d, v = self.mem.read4(self.jvm.ar)
        self.signals.append(self.jvm.alu_op(read=True, source='opr', destination='ac', isext8='ac'))
        self.delays.append(d)
        self.print_status()

        # dr_self.mem
        self.signals.append(self.jvm.alu_op(assign=('dr', v)))
        self.delays.append(0)
        self.print_status()

        # dr_ld data_dr alu_sum
        self.signals.append(self.jvm.alu_op(source='dr', operation='sum', destination='dr'))
        self.delays.append(0)
        self.print_status()

        # m_write
        d = self.mem.write(self.jvm.ar, self.jvm.dr)
        self.signals.append(self.jvm.alu_op(write=True))
        self.delays.append(d)
        self.print_status()

    def goto(self):
        # m_read ar_inc pc_dec
        d, v = self.mem.read1(self.jvm.ar)
        self.signals.append(self.jvm.alu_op(incrementby1=['ar'], decrementby1=['pc'], read=True))
        self.delays.append(d)
        self.print_status()

        # opr_ld m_read
        d1, v1 = self.mem.read1(self.jvm.ar)
        self.signals.append(self.jvm.alu_op(assign=('opr', v), read=True))
        self.delays.append(d1)
        self.print_status()

        # r_ld data_opr shifter_8 opr_ld
        self.signals.append(self.jvm.alu_op(source='opr', shamt=8, destination='ac', assign=('opr', v1)))
        self.delays.append(0)
        self.print_status()

        # r_ld data_opr alu_or r_isext16
        self.signals.append(self.jvm.alu_op(destination='ac', source='opr', operation='or', isext16='ac'))
        self.delays.append(0)
        self.print_status()

        # pc_ld data_pc alu_sum
        self.signals.append(self.jvm.alu_op(destination='pc', source='pc', operation='sum'))
        self.delays.append(0)
        self.print_status()


    def popjump(self):
        #ar_ld data_sp sp_dec4
        self.signals.append(self.jvm.alu_op(source='sp', destination='ar', decrementby4='sp'))
        self.delays.append(0)
        self.print_status()

        #m_read ar_ld data_pc
        d, v = self.mem.read4(self.jvm.ar)
        self.signals.append(self.jvm.alu_op(read=True, destination='ar', source='pc'))
        self.delays.append(d)
        self.print_status()

        #dr_mem
        self.signals.append(self.jvm.alu_op(assign=('dr', v)))
        self.delays.append(0)
        self.print_status()

        if self.jvm.ir == opcode_dict['ifeq']:
            #br_ld data_dr
            self.signals.append(self.jvm.alu_op(source='dr', s_z = True))
        else:
            #br_ld n_sel data_dr
            self.signals.append(self.jvm.alu_op(source='dr', s_n = True))
        self.delays.append(0)
        self.print_status()

        if self.jvm.b:
            self.goto()
        else:
            # pc_inc
            self.signals.append(self.jvm.alu_op(incrementby1='pc'))
            self.delays.append(0)
            self.print_status()

            # pc_inc
            self.signals.append(self.jvm.alu_op(incrementby1='pc'))
            self.delays.append(0)
            self.print_status()

    def pop2jump(self):
        #ifimpeq: ar_ld data_sp sp_dec4
        self.signals.append(self.jvm.alu_op(source='sp', destination='ar', decrementby4='sp'))
        self.delays.append(0)
        self.print_status()

        #m_read ar_dec4 sp_dec4
        d, v = self.mem.read4(self.jvm.ar)
        self.signals.append(self.jvm.alu_op(read = True, decrementby4=['sp', 'ar']))
        self.delays.append(d)
        self.print_status()

        #dr_mem m_read ar_ld data_pc
        d1, v1 = self.mem.read4(self.jvm.ar)
        self.signals.append(self.jvm.alu_op(assign=('dr', v), read = True, source='pc', destination='ar'))
        self.delays.append(d1)
        self.print_status()

        #r_ld data_dr dr_mem
        self.signals.append(self.jvm.alu_op(destination='ac', source='dr', assign=('dr', v1)))
        self.delays.append(0)
        self.print_status()

        # data_dr alu_sub br_ld z_sel
        self.signals.append(self.jvm.alu_op(source='dr', operation='sub', s_z=True))
        self.delays.append(0)
        self.print_status()

        if self.jvm.b:
            # goto goto
            self.goto()

        else:
            # pc_inc
            self.signals.append(self.jvm.alu_op(incrementby1='pc'))
            self.delays.append(0)
            self.print_status()

            # pc_inc
            self.signals.append(self.jvm.alu_op(incrementby1='pc'))
            self.delays.append(0)
            self.print_status()




