import math
import random
import mem
from bitstring import Bits

tmp = 0

class cache:

    def __init__(self, memory, size=128, setSize=8, policy='FIFO', writePolicy='direct'):
        self.size = size
        self.policy = policy
        self.writePolicy = writePolicy
        self.data = [0] * size
        self.tag = [0] * size
        self.dirty = [False] * size
        self.valid = [False] * size
        self.lastUsage = [0] * size
        self.insertTime = [0] * size
        self.delayRatio = 1
        self.blockSize = 1
        self.setSize = setSize
        self.setNumber = size / setSize
        self.currentStep = 0
        self.hitCount = 0
        self.missCount = 0
        self.memory = memory

    def cacheAddressMap(self, rawAddress):
        return ((rawAddress/4) % self.setNumber) * self.setSize

    def isInCache(self, rawAddress):

        baseAddress = self.cacheAddressMap(rawAddress)
        for i in range(int(self.setSize/4)):
            index = int(baseAddress + i * 4)
            if self.tag[index] == self.findTag(rawAddress) and self.valid[index]:
                return index

        return -1

    def findTag(self, rawAddress):
        # print("SADFFSADFSASFD")
        # print(rawAddress)
        # print(self.setNumber)
        return int((rawAddress/4)/self.setNumber)

    def remakeAddress(self, tag, cacheAddress):
        return (tag * self.setNumber + (cacheAddress/self.setSize)) * 4

    def evictionFIFO(self, baseAddress):
        ret = int(baseAddress)
        for i in range(int(self.setSize/4)):
            index = int(baseAddress + i * 4)
            if self.valid[index] == False or self.insertTime[index] < self.insertTime[ret]:
                ret = int(baseAddress + i * 4)
                if self.valid[ret] == False:
                    return ret
        return ret

    def evictionLRU(self, baseAddress):
        ret = int(baseAddress)
        for i in range(int(self.setSize/4)):
            index = int(baseAddress + i * 4)
            if self.valid[index] == False or self.lastUsage[index] < self.lastUsage[ret]:
                ret = int(baseAddress + i * 4)
                if self.valid[ret] == False:
                    return ret
        return ret

    def evictionMRU(self, baseAddress):
        ret = int(baseAddress)
        for i in range(int(self.setSize/4)):
            index = int(baseAddress + i * 4)
            if self.valid[index] == False or self.lastUsage[index] > self.lastUsage[ret]:
                ret = int(baseAddress + i * 4)
                if not self.valid[int(ret)]:
                    return ret
        return ret

    def evictionRandom(self, baseAddresS):
        return baseAddresS + math.floor(random.random()*int(self.setSize/4)) * 4

    def eviction(self, baseAddress):
        exec('global tmp; tmp = self.eviction' + self.policy + '(' + str(baseAddress) + ')')
        return tmp



    def fast_read4(self, address):
        self.is_read4 = True
        return self.read4(address)[1]


    def read4(self, rawAddress):



        # print("------------------------------------------")
        print("read: " + str(rawAddress))
        # print(self.data)
        # print(self.tag)

        self.currentStep += 1
        baseAddress = self.cacheAddressMap(rawAddress)

        finalAddress = baseAddress
        # print("searching for data in address " + str(rawAddress))
        for i in range(int(self.setSize/4)):
            index = int(baseAddress + i*4)

            if self.tag[index] == self.findTag(rawAddress) and self.valid[index]:
                self.lastUsage[index] = self.currentStep
                self.hitCount += 1
                print("Hit for data read in address " + str(rawAddress))
                return [math.floor(random.random()*4+1) * self.delayRatio,
                            Bits(bin=
                             Bits(int=self.data[index], length=8).bin +
                             Bits(int=self.data[index + 1], length=8).bin +
                             Bits(int=self.data[index + 2], length=8).bin +
                             Bits(int=self.data[index + 3], length=8).bin
                             ).int]

        address = int(self.eviction(self.cacheAddressMap(rawAddress)))

        d2 = 0
        if(self.dirty[address]):
            for k in range(4):
                self.dirty[address + k] = False
            d2 = self.memory.write(self.remakeAddress(self.tag[address], address),
                                   self.data[address] * (2 ** 24) +
                                   self.data[address + 1] * (2 ** 16) +
                                   self.data[address + 2] * (2 ** 8) +
                                   self.data[address + 3]
                                   )

        d1, read_value = (self.memory.read4(rawAddress))
        read_value = Bits(int=read_value, length=32).bin

        b_data = [0]*4
        for k in range(4):
            b_data[k] = Bits(bin=read_value[k*8:k*8+8]).int
            self.data[address + k] = b_data[k]

        for k in range(4):
            self.tag[address + k] = int(self.findTag(rawAddress))
            self.insertTime[address + k] = self.currentStep
            self.lastUsage[address + k] = self.currentStep
            self.valid[address + k] = True
            self.dirty[address + k] = False

        self.missCount += 1
        totalDelay = math.floor(random.random()*4+1) * self.delayRatio + d1 + d2
        print("Miss for data read in address " + str(rawAddress))

        return [totalDelay, Bits(bin=read_value).int]


    #in case of miss, read1 fetches 4bytes and returns needed one, So adjacent bytes would be also loaded into cache:
    def read1(self, rawAddress):
        return self.memory.read1(rawAddress)

        xyz = rawAddress

        md = rawAddress % 4
        rawAddress = 4 * (int(rawAddress/4))
        # print(rawAddress)
        td, dd = self.read4(rawAddress)
        # print(dd)
        # print(Bits(int=dd, length=32).bin)
        # print(rawAddress)
        # print([td, Bits(bin=(Bits(int=dd, length=32).bin)[8*md:8*md + 8]).int])
        # print("\n\n")

        return self.memory.read1(xyz)
        return [td, Bits(bin=(Bits(uint=dd, length=32).bin)[8*md:8*md + 8]).int]


    def write(self, rawAddress, value):
        # print("------------------------------------------")
        print("write: " + str(rawAddress) + " " + str(value))
        # print(self.data)
        # print(self.tag)


        b_value = [0] * 4
        for k in range(4):
            b_value[k] = Bits(bin=(Bits(int=value, length=32).bin[k*8:k*8+8])).int


        self.currentStep += 1
        address = int(self.isInCache(rawAddress))

        if address != -1:
            for k in range(4):
                self.data[address + k] = b_value[k]
                self.dirty[address + k] = True
                self.valid[address + k] = True
                self.lastUsage[address + k] = self.currentStep
                self.hitCount += 1
            print("Hit for data write in address " + str(rawAddress))

            return math.floor(random.random()*4+1) * self.delayRatio


        address = int(self.eviction(self.cacheAddressMap(rawAddress)))
        d1 = 0

        if self.dirty[address]:
            for k in range(4):
                self.dirty[address + k] = False
            d1 = self.memory.write(self.remakeAddress(self.tag[address], address),
                                   self.data[address] * (2 ** 24) +
                                   self.data[address + 1] * (2 ** 16) +
                                   self.data[address + 2] * (2 ** 8) +
                                   self.data[address + 3]
                                   )

        d2 = 0
        if self.writePolicy == 'direct':
            d2 = self.memory.write(rawAddress, value)

            for k in range(4):
                self.dirty[address + k] = False

        if self.writePolicy == 'indirect':
            for k in range(4):
                self.dirty[address + k] = True

        for k in range(4):
            self.data[address + k] = b_value[k]
            self.insertTime[address + k] = self.currentStep
            self.lastUsage[address + k] = self.currentStep
            self.tag[address + k] = int(self.findTag(rawAddress))
            self.valid[address + k] = True

        self.missCount += 1
        print("Miss for data write in address " + str(rawAddress))

        return math.floor(random.random()*4+1) * self.delayRatio + d1 + d2

memory = mem.mem(size=32, delay_ratio=10, d = [1,2,3,4,5,6,7,8])
c = cache(memory, 16, 4, 'FIFO', 'direct')

# print(memory.data)
# print(c.data)
# print(c.tag)
# print(c.read1(0))
# print(c.read1(1))
# print(c.read1(2))
# print(c.read4(0))
# print(memory.read4(4))
#
# print(c.data)
# print(c.tag)
# print(memory.read4(0))
# print(c.read4(4))
#
# # print(c.write(0,17))
#
# print(c.data)
# print(c.tag)

# test for FIFO/direct:
# print(c.write(60, 101))
# print(c.write(124, 102))
# print(c.write(188, 103))
# print(c.write(252, 105))
# print(c.write(316, 104))
# print(c.write(124, 109))    #this line has no memory access so a small delay
# print(c.write(316, 117))    #this line has no memory access too
# print(c.write(60, 117))     #this line should write a dirty value into the cache so has a double delay(value in cell#124)
# print(c.write(124, 117))    #cell#124 is replaced so we have a memory access again
# print(c.read4(60))          #hitted reading
# print(c.read4(67))          #missd reading
